FROM php:7-fpm-buster

WORKDIR /var/www
COPY --chown=www-data:www-data . /var/www

RUN apt-get update && \
    apt-get install -y --no-install-recommends nginx wget libzip-dev && \
    cat docker/nginx-site.conf > /etc/nginx/sites-enabled/default && \
    docker-php-ext-install zip mysqli && \
    nginx -t && \
    wget https://getcomposer.org/composer-stable.phar -O /usr/bin/composer && \
    chmod +x /usr/bin/composer && \
    composer install && \
    rm -rf /var/www/html /var/lib/apt/lists/*

CMD ["/var/www/docker/entrypoint"]

EXPOSE 80

